package br.fcv.spring_jooq_poc

import br.fcv.spring_jooq_poc.database.tables.pojos.Users
import br.fcv.spring_jooq_poc.database.tables.references.USERS
import org.assertj.core.api.Assertions.assertThat
import org.jooq.DSLContext
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.transaction.annotation.Transactional
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import java.time.Clock
import java.time.Instant
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZoneOffset.UTC

@SpringBootTest
@Testcontainers
@Transactional
internal class UserServiceTest @Autowired constructor(
    private val context: DSLContext,
) {

    private fun newUserService(clock: Clock = Clock.systemUTC()): UserService = UserService(context, clock)

    @Test
    fun listAll() {
        val count = context.insertInto(USERS)
            .columns(USERS.NAME, USERS.CREATED_AT, USERS.DATE_OF_BIRTH)
            .values("John Doe", OffsetDateTime.parse("2021-10-19T13:23:52Z"), LocalDate.parse("1980-10-26"))
            .values("Jane Doe", OffsetDateTime.parse("2021-10-19T13:24:05Z"), LocalDate.parse("1983-02-15"))
            .execute()

        assertThat(count).isEqualTo(2)

        val userService = newUserService()
        val users = userService.listAll().map { withOffsetSameInstantAtUTC(it) }

        assertThat(users)
            .usingRecursiveFieldByFieldElementComparatorIgnoringFields("id")
            .containsExactly(
                Users(
                    id = null,
                    name = "John Doe",
                    createdAt = OffsetDateTime.parse("2021-10-19T13:23:52Z"),
                    dateOfBirth = LocalDate.parse("1980-10-26"),
                ),
                Users(
                    id = null,
                    name = "Jane Doe",
                    createdAt = OffsetDateTime.parse("2021-10-19T13:24:05Z"),
                    dateOfBirth = LocalDate.parse("1983-02-15"),
                ),
            )
    }

    @Test
    fun listMinors() {
        val count = context.insertInto(USERS)
            .columns(USERS.NAME, USERS.CREATED_AT, USERS.DATE_OF_BIRTH)
            .values("John Doe", OffsetDateTime.parse("2021-10-19T13:23:52Z"), LocalDate.parse("1980-10-26"))
            .values("Jane Doe", OffsetDateTime.parse("2021-10-19T13:24:05Z"), LocalDate.parse("1983-02-15"))
            .execute()

        assertThat(count).isEqualTo(2)

        val userService = newUserService(
            clock = Clock.fixed(Instant.parse("1999-10-19T15:06:14Z"), UTC)
        )

        val allUsers = userService.listAll()
        val minorUsers = userService.listMinors().map { withOffsetSameInstantAtUTC(it) }

        assertThat(allUsers.map { it.name} ).containsExactly("John Doe", "Jane Doe")
        assertThat(minorUsers)
            .usingRecursiveFieldByFieldElementComparatorIgnoringFields("id")
            .containsExactly(
                Users(
                    id = null,
                    name = "Jane Doe",
                    createdAt = OffsetDateTime.parse("2021-10-19T13:24:05Z"),
                    dateOfBirth = LocalDate.parse("1983-02-15"),
                ),
            )
    }

    // OffsetDateTime are restored from DB using JVM's default Time Zone at the moment the Connections are
    // established [1][2], manually converting it to UTC to avoid relying on globally modifiable values
    //
    // [1]: https://github.com/pgjdbc/pgjdbc/blob/REL42.2.19/pgjdbc/src/main/java/org/postgresql/core/v3/ConnectionFactoryImpl.java#L393
    // [2]: https://github.com/pgjdbc/pgjdbc/blob/REL42.2.19/pgjdbc/src/main/java/org/postgresql/core/v3/ConnectionFactoryImpl.java#L340
    private fun withOffsetSameInstantAtUTC(user: Users): Users =
        user.copy(createdAt = user.createdAt?.withOffsetSameInstant(UTC))

    companion object {
        // Consider keeping PostgreSQL image version here in sync with docker-compose.yml's postgres service version
        @Container
        private val db = SpringJooqPocPostgreSQLContainer()

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) = registerDatasourceProperties(db, registry)
    }

}

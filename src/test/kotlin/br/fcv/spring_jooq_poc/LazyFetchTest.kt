package br.fcv.spring_jooq_poc

import org.assertj.core.api.Assertions.assertThat
import org.jooq.DSLContext
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlConfig
import org.springframework.transaction.annotation.Transactional
import org.testcontainers.containers.output.Slf4jLogConsumer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@SpringBootTest
@Testcontainers
@Transactional
// Overriding SQL separator in order to prevent an error like:
// > org.postgresql.util.PSQLException: Unterminated dollar quote started at position 52 in SQL CREATE OR REPLACE FUNCTION echo(int) RETURNS int AS $BODY$ BEGIN RAISE LOG 'echo(%)', $1. Expected terminating $$
// Similarly as in https://stackoverflow.com/a/57851978/301686
@Sql(
    scripts = [ "classpath:br/fcv/spring_jooq_poc/LazyFetchTest.create-echo-function.sql" ],
    config = SqlConfig(
        separator = ";;"
    )
)
internal class LazyFetchTest @Autowired constructor(
    private val ctx: DSLContext,
) {

    private val logger = LoggerFactory.getLogger(LazyFetchTest::class.java)

    @Test
    fun stream() {
        val stream = ctx.resultQuery("SELECT echo(v) FROM generate_series(1, 20) v")
            .fetchSize(5)
            .stream()
        val mappedStream = stream.map { it.get(0, Integer::class.java) }
        val limitedStream = mappedStream.limit(7)

        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] LOG:  execute <unnamed>/C_4: SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] LOG:  echo(1)
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] LOG:  echo(2)
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] LOG:  echo(3)
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] LOG:  echo(4)
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] LOG:  echo(5)
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.674 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.670 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] LOG:  execute fetch from <unnamed>/C_4: SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] LOG:  echo(6)
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] LOG:  echo(7)
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] LOG:  echo(8)
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.696 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] LOG:  echo(9)
        // 14:11:33.697 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.697 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:11:33.697 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] LOG:  echo(10)
        // 14:11:33.697 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:11:33.697 [docker-java-stream--1361051470] ERROR postgres -- 2023-03-26 12:11:33.692 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        val materializedList = limitedStream.toList()

        assertThat(materializedList).isEqualTo((1..7).toList())
    }

    @Test
    fun streamConsumedIndividually() {
        val stream = ctx.resultQuery("SELECT echo(v) FROM generate_series(1, 20) v")
            .fetchSize(5)
            .stream()
        val mappedStream = stream.map { it.get(0, Int::class.java) }
        val limitedStream = mappedStream.limit(7)

        val mutableList = mutableListOf<Int>()
        // 14:22:09.114 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.104 UTC [68][3/31][729] LOG:  execute <unnamed>/C_4: SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] LOG:  echo(1)
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] LOG:  echo(2)
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] LOG:  echo(3)
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] LOG:  echo(4)
        // 14:22:09.115 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:09.116 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:09.116 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] LOG:  echo(5)
        // 14:22:09.116 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:09.116 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:09.108 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:11.197 [Test worker] INFO  br.fcv.spring_jooq_poc.LazyFetchTest -- Consumed value 1
        // 14:22:11.711 [Test worker] INFO  br.fcv.spring_jooq_poc.LazyFetchTest -- Consumed value 2
        // 14:22:12.086 [Test worker] INFO  br.fcv.spring_jooq_poc.LazyFetchTest -- Consumed value 3
        // 14:22:12.469 [Test worker] INFO  br.fcv.spring_jooq_poc.LazyFetchTest -- Consumed value 4
        // 14:22:12.861 [Test worker] INFO  br.fcv.spring_jooq_poc.LazyFetchTest -- Consumed value 5
        // 14:22:13.108 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.097 UTC [68][3/31][729] LOG:  execute fetch from <unnamed>/C_4: SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.097 UTC [68][3/31][729] LOG:  echo(6)
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.097 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.097 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.098 UTC [68][3/31][729] LOG:  echo(7)
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.098 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.098 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.098 UTC [68][3/31][729] LOG:  echo(8)
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.098 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.098 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:13.109 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.098 UTC [68][3/31][729] LOG:  echo(9)
        // 14:22:13.110 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.098 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:13.110 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.098 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:13.110 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.101 UTC [68][3/31][729] LOG:  echo(10)
        // 14:22:13.110 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.101 UTC [68][3/31][729] CONTEXT:  PL/pgSQL function echo(integer) line 1 at RAISE
        // 14:22:13.110 [docker-java-stream--939232838] ERROR postgres -- 2023-03-26 12:22:13.101 UTC [68][3/31][729] STATEMENT:  SELECT echo(v) FROM generate_series(1, 20) v
        // 14:22:13.509 [Test worker] INFO  br.fcv.spring_jooq_poc.LazyFetchTest -- Consumed value 6
        // 14:22:14.412 [Test worker] INFO  br.fcv.spring_jooq_poc.LazyFetchTest -- Consumed value 7
        for (i in limitedStream) {
            logger.info("Consumed value {}", i)
            mutableList.add(i)
        }

        assertThat(mutableList).isEqualTo((1..7).toList())
    }

    companion object {
        @Container
        private val db = SpringJooqPocPostgreSQLContainer()
            .withCommand(
                "postgres",
                // Log all statements at server side
                // See more at https://www.postgresql.org/docs/15/runtime-config-logging.html#GUC-LOG-STATEMENT
                "-c",
                "log_statement=all",
                // Customize the log line prefix in order to print information about the current transaction.
                // See more about log_line_prefix parameter at
                // https://www.postgresql.org/docs/15/runtime-config-logging.html#GUC-LOG-LINE-PREFIX
                //
                // List of possible "escape" tokens:
                // %a  Application name
                // %u  User name
                // %d  Database name
                // %r  Remote host name or IP address, and remote port
                // %h  Remote host name or IP address
                // %p  Process ID
                // %t  Time stamp without milliseconds
                // %m  Time stamp with milliseconds
                // %n  Time stamp with milliseconds (as a Unix epoch)
                // %i  Command tag: type of session's current command
                // %e  SQLSTATE error code
                // %c  Session ID: see below
                // %l  Number of the log line for each session or process, starting at 1
                // %s  Process start time stamp
                // %v  Virtual transaction ID (backendID/localXID)
                // %x  Transaction ID (0 if none is assigned)
                // %q  Produces no output, but tells non-session processes to stop at this point in the string; ignored by session processes
                "-c",
                "log_line_prefix=%m [%p][%v][%x] "
            )
            .withLogConsumer(
                Slf4jLogConsumer(
                        LoggerFactory.getLogger("postgres"),
                true
                )
            )

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) = registerDatasourceProperties(db, registry)
    }
}

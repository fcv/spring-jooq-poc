package br.fcv.spring_jooq_poc

import org.springframework.test.context.DynamicPropertyRegistry
import org.testcontainers.containers.PostgreSQLContainer

internal class SpringJooqPocPostgreSQLContainer : PostgreSQLContainer<SpringJooqPocPostgreSQLContainer>(
    // Consider keeping PostgreSQL image version here in sync with docker-compose.yml's postgres service version used
    // at development time
    "postgres:17.2"
)

internal fun registerDatasourceProperties(container: SpringJooqPocPostgreSQLContainer, registry: DynamicPropertyRegistry) {
    with(registry) {
        add("spring.datasource.url", container::getJdbcUrl)
        add("spring.datasource.username", container::getUsername)
        add("spring.datasource.password", container::getPassword)
    }
}

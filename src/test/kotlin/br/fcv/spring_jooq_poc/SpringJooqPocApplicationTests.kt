package br.fcv.spring_jooq_poc

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers

@SpringBootTest
@Testcontainers
class SpringJooqPocApplicationTests {

	@Test
	fun contextLoads() {
	}

	companion object {
		// Consider keeping PostgreSQL image version here in sync with docker-compose.yml's postgres service version
		@Container
		private val db = SpringJooqPocPostgreSQLContainer()

		@DynamicPropertySource
		@JvmStatic
		fun registerDynamicProperties(registry: DynamicPropertyRegistry) = registerDatasourceProperties(db, registry)
	}

}

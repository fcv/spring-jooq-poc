/*
 * This file is generated by jOOQ.
 */
package br.fcv.spring_jooq_poc.database.tables


import br.fcv.spring_jooq_poc.database.Public
import br.fcv.spring_jooq_poc.database.keys.APPOINTMENTS_PKEY
import br.fcv.spring_jooq_poc.database.keys.APPOINTMENTS__APPOINTMENT_USER_FK
import br.fcv.spring_jooq_poc.database.tables.Users.UsersPath
import br.fcv.spring_jooq_poc.database.tables.records.AppointmentsRecord

import java.time.OffsetDateTime
import java.util.function.Function

import kotlin.collections.Collection
import kotlin.collections.List

import org.jooq.Condition
import org.jooq.Field
import org.jooq.ForeignKey
import org.jooq.Identity
import org.jooq.InverseForeignKey
import org.jooq.Name
import org.jooq.Path
import org.jooq.PlainSQL
import org.jooq.QueryPart
import org.jooq.Record
import org.jooq.Records
import org.jooq.Row4
import org.jooq.SQL
import org.jooq.Schema
import org.jooq.Select
import org.jooq.SelectField
import org.jooq.Stringly
import org.jooq.Table
import org.jooq.TableField
import org.jooq.TableOptions
import org.jooq.UniqueKey
import org.jooq.impl.DSL
import org.jooq.impl.Internal
import org.jooq.impl.SQLDataType
import org.jooq.impl.TableImpl


/**
 * This class is generated by jOOQ.
 */
@Suppress("UNCHECKED_CAST")
open class Appointments(
    alias: Name,
    path: Table<out Record>?,
    childPath: ForeignKey<out Record, AppointmentsRecord>?,
    parentPath: InverseForeignKey<out Record, AppointmentsRecord>?,
    aliased: Table<AppointmentsRecord>?,
    parameters: Array<Field<*>?>?,
    where: Condition?
): TableImpl<AppointmentsRecord>(
    alias,
    Public.PUBLIC,
    path,
    childPath,
    parentPath,
    aliased,
    parameters,
    DSL.comment(""),
    TableOptions.table(),
    where,
) {
    companion object {

        /**
         * The reference instance of <code>public.appointments</code>
         */
        val APPOINTMENTS: Appointments = Appointments()
    }

    /**
     * The class holding records for this type
     */
    override fun getRecordType(): Class<AppointmentsRecord> = AppointmentsRecord::class.java

    /**
     * The column <code>public.appointments.id</code>.
     */
    val ID: TableField<AppointmentsRecord, Int?> = createField(DSL.name("id"), SQLDataType.INTEGER.nullable(false).identity(true), this, "")

    /**
     * The column <code>public.appointments.start_at</code>.
     */
    val START_AT: TableField<AppointmentsRecord, OffsetDateTime?> = createField(DSL.name("start_at"), SQLDataType.TIMESTAMPWITHTIMEZONE(6), this, "")

    /**
     * The column <code>public.appointments.end_at</code>.
     */
    val END_AT: TableField<AppointmentsRecord, OffsetDateTime?> = createField(DSL.name("end_at"), SQLDataType.TIMESTAMPWITHTIMEZONE(6), this, "")

    /**
     * The column <code>public.appointments.user_id</code>.
     */
    val USER_ID: TableField<AppointmentsRecord, Int?> = createField(DSL.name("user_id"), SQLDataType.INTEGER.nullable(false), this, "")

    private constructor(alias: Name, aliased: Table<AppointmentsRecord>?): this(alias, null, null, null, aliased, null, null)
    private constructor(alias: Name, aliased: Table<AppointmentsRecord>?, parameters: Array<Field<*>?>?): this(alias, null, null, null, aliased, parameters, null)
    private constructor(alias: Name, aliased: Table<AppointmentsRecord>?, where: Condition?): this(alias, null, null, null, aliased, null, where)

    /**
     * Create an aliased <code>public.appointments</code> table reference
     */
    constructor(alias: String): this(DSL.name(alias))

    /**
     * Create an aliased <code>public.appointments</code> table reference
     */
    constructor(alias: Name): this(alias, null)

    /**
     * Create a <code>public.appointments</code> table reference
     */
    constructor(): this(DSL.name("appointments"), null)

    constructor(path: Table<out Record>, childPath: ForeignKey<out Record, AppointmentsRecord>?, parentPath: InverseForeignKey<out Record, AppointmentsRecord>?): this(Internal.createPathAlias(path, childPath, parentPath), path, childPath, parentPath, APPOINTMENTS, null, null)

    /**
     * A subtype implementing {@link Path} for simplified path-based joins.
     */
    open class AppointmentsPath : Appointments, Path<AppointmentsRecord> {
        constructor(path: Table<out Record>, childPath: ForeignKey<out Record, AppointmentsRecord>?, parentPath: InverseForeignKey<out Record, AppointmentsRecord>?): super(path, childPath, parentPath)
        private constructor(alias: Name, aliased: Table<AppointmentsRecord>): super(alias, aliased)
        override fun `as`(alias: String): AppointmentsPath = AppointmentsPath(DSL.name(alias), this)
        override fun `as`(alias: Name): AppointmentsPath = AppointmentsPath(alias, this)
        override fun `as`(alias: Table<*>): AppointmentsPath = AppointmentsPath(alias.qualifiedName, this)
    }
    override fun getSchema(): Schema? = if (aliased()) null else Public.PUBLIC
    override fun getIdentity(): Identity<AppointmentsRecord, Int?> = super.getIdentity() as Identity<AppointmentsRecord, Int?>
    override fun getPrimaryKey(): UniqueKey<AppointmentsRecord> = APPOINTMENTS_PKEY
    override fun getReferences(): List<ForeignKey<AppointmentsRecord, *>> = listOf(APPOINTMENTS__APPOINTMENT_USER_FK)

    private lateinit var _users: UsersPath

    /**
     * Get the implicit join path to the <code>public.users</code> table.
     */
    fun users(): UsersPath {
        if (!this::_users.isInitialized)
            _users = UsersPath(this, APPOINTMENTS__APPOINTMENT_USER_FK, null)

        return _users;
    }

    val users: UsersPath
        get(): UsersPath = users()
    override fun `as`(alias: String): Appointments = Appointments(DSL.name(alias), this)
    override fun `as`(alias: Name): Appointments = Appointments(alias, this)
    override fun `as`(alias: Table<*>): Appointments = Appointments(alias.qualifiedName, this)

    /**
     * Rename this table
     */
    override fun rename(name: String): Appointments = Appointments(DSL.name(name), null)

    /**
     * Rename this table
     */
    override fun rename(name: Name): Appointments = Appointments(name, null)

    /**
     * Rename this table
     */
    override fun rename(name: Table<*>): Appointments = Appointments(name.qualifiedName, null)

    /**
     * Create an inline derived table from this table
     */
    override fun where(condition: Condition?): Appointments = Appointments(qualifiedName, if (aliased()) this else null, condition)

    /**
     * Create an inline derived table from this table
     */
    override fun where(conditions: Collection<Condition>): Appointments = where(DSL.and(conditions))

    /**
     * Create an inline derived table from this table
     */
    override fun where(vararg conditions: Condition?): Appointments = where(DSL.and(*conditions))

    /**
     * Create an inline derived table from this table
     */
    override fun where(condition: Field<Boolean?>?): Appointments = where(DSL.condition(condition))

    /**
     * Create an inline derived table from this table
     */
    @PlainSQL override fun where(condition: SQL): Appointments = where(DSL.condition(condition))

    /**
     * Create an inline derived table from this table
     */
    @PlainSQL override fun where(@Stringly.SQL condition: String): Appointments = where(DSL.condition(condition))

    /**
     * Create an inline derived table from this table
     */
    @PlainSQL override fun where(@Stringly.SQL condition: String, vararg binds: Any?): Appointments = where(DSL.condition(condition, *binds))

    /**
     * Create an inline derived table from this table
     */
    @PlainSQL override fun where(@Stringly.SQL condition: String, vararg parts: QueryPart): Appointments = where(DSL.condition(condition, *parts))

    /**
     * Create an inline derived table from this table
     */
    override fun whereExists(select: Select<*>): Appointments = where(DSL.exists(select))

    /**
     * Create an inline derived table from this table
     */
    override fun whereNotExists(select: Select<*>): Appointments = where(DSL.notExists(select))

    // -------------------------------------------------------------------------
    // Row4 type methods
    // -------------------------------------------------------------------------
    override fun fieldsRow(): Row4<Int?, OffsetDateTime?, OffsetDateTime?, Int?> = super.fieldsRow() as Row4<Int?, OffsetDateTime?, OffsetDateTime?, Int?>

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Function)}.
     */
    fun <U> mapping(from: (Int?, OffsetDateTime?, OffsetDateTime?, Int?) -> U): SelectField<U> = convertFrom(Records.mapping(from))

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Class,
     * Function)}.
     */
    fun <U> mapping(toType: Class<U>, from: (Int?, OffsetDateTime?, OffsetDateTime?, Int?) -> U): SelectField<U> = convertFrom(toType, Records.mapping(from))
}

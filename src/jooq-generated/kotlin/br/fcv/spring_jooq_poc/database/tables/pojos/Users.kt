/*
 * This file is generated by jOOQ.
 */
package br.fcv.spring_jooq_poc.database.tables.pojos


import java.io.Serializable
import java.time.LocalDate
import java.time.OffsetDateTime


/**
 * This class is generated by jOOQ.
 */
@Suppress("UNCHECKED_CAST")
data class Users(
    val id: Int? = null,
    val name: String? = null,
    val createdAt: OffsetDateTime? = null,
    val dateOfBirth: LocalDate? = null
): Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other)
            return true
        if (other == null)
            return false
        if (this::class != other::class)
            return false
        val o: Users = other as Users
        if (this.id == null) {
            if (o.id != null)
                return false
        }
        else if (this.id != o.id)
            return false
        if (this.name == null) {
            if (o.name != null)
                return false
        }
        else if (this.name != o.name)
            return false
        if (this.createdAt == null) {
            if (o.createdAt != null)
                return false
        }
        else if (this.createdAt != o.createdAt)
            return false
        if (this.dateOfBirth == null) {
            if (o.dateOfBirth != null)
                return false
        }
        else if (this.dateOfBirth != o.dateOfBirth)
            return false
        return true
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + (if (this.id == null) 0 else this.id.hashCode())
        result = prime * result + (if (this.name == null) 0 else this.name.hashCode())
        result = prime * result + (if (this.createdAt == null) 0 else this.createdAt.hashCode())
        result = prime * result + (if (this.dateOfBirth == null) 0 else this.dateOfBirth.hashCode())
        return result
    }

    override fun toString(): String {
        val sb = StringBuilder("Users (")

        sb.append(id)
        sb.append(", ").append(name)
        sb.append(", ").append(createdAt)
        sb.append(", ").append(dateOfBirth)

        sb.append(")")
        return sb.toString()
    }
}

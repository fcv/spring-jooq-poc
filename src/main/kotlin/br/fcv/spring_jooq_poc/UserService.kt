package br.fcv.spring_jooq_poc

import br.fcv.spring_jooq_poc.database.tables.pojos.Users
import br.fcv.spring_jooq_poc.database.tables.references.USERS
import org.jooq.Condition
import org.jooq.DSLContext
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Clock
import java.time.LocalDate
import java.time.OffsetDateTime

@Service
class UserService(private val context: DSLContext, private val clock: Clock) {

  @Transactional(readOnly = true)
  fun listAll(): List<Users> =
    // Unfortunately jOOQ generates POJOs with all properties as nullable, regardless whether the database's columns are
    // nullable or not
    // see https://github.com/jOOQ/jOOQ/issues/10212
    context.selectFrom(USERS)
      .fetchInto(Users::class.java)

  @Transactional(readOnly = true)
  fun listMinors(): List<Users> =
    context.selectFrom(USERS)
      .where(isUserMinor(clock))
      .fetchInto(Users::class.java)

  private fun isUserMinor(clock: Clock): Condition {
    val minorAge = 18L
    val today = LocalDate.now(clock)
    // Strangely jOOQ doesn't require an instance of the table used in the SELECT clause to create a condition,
    // so theoretically it would be possible to create a condition on a table that is not in the SELECT clause
    return USERS.DATE_OF_BIRTH.ge(today.minusYears(minorAge))
  }

  @Transactional
  fun createUser(name: String, dateOfBirth: LocalDate): Users {
    val createdAt = OffsetDateTime.now(clock)
    val queryResult = context.insertInto(USERS, USERS.NAME, USERS.DATE_OF_BIRTH, USERS.CREATED_AT)
      .values(name, dateOfBirth, createdAt)
      .returning(USERS.ID)
      .fetchOne()

    return queryResult?.id
      ?.let { id ->
        Users(id, name, createdAt, dateOfBirth)
      }
      ?: throw IllegalStateException("Insert resulted on empty result set?")
  }
}

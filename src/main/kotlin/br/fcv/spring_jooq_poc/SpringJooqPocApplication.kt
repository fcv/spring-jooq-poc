package br.fcv.spring_jooq_poc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringJooqPocApplication

fun main(args: Array<String>) {
	runApplication<SpringJooqPocApplication>(*args)
}

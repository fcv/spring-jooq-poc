package br.fcv.spring_jooq_poc.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Clock

@Configuration
class ClockConfiguration {
  @Bean
  fun defaultClock(): Clock = Clock.systemUTC()
}

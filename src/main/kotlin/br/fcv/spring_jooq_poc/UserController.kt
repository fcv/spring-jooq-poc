package br.fcv.spring_jooq_poc

import br.fcv.spring_jooq_poc.database.tables.pojos.Users
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
@RequestMapping("users")
class UserController(val service: UserService) {

  @GetMapping
  fun get(@RequestParam minorsOnly: Boolean?): List<Users> =
    if (minorsOnly == true) {
      service.listMinors()
    } else {
      service.listAll()
    }

  @PostMapping
  fun post(@RequestBody newUserRequest: NewUserRequest): Users =
    service.createUser(newUserRequest.name, newUserRequest.dateOfBirth)

  data class NewUserRequest(val name: String, val dateOfBirth: LocalDate)
}

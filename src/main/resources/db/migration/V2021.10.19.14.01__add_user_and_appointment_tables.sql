CREATE TABLE users (
    id SERIAL PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    date_of_birth DATE NOT NULL
);

CREATE TABLE appointments (
    id SERIAL PRIMARY KEY NOT NULL,
    start_at TIMESTAMP WITH TIME ZONE,
    end_at TIMESTAMP WITH TIME ZONE,
    user_id INT NOT NULL,
    CONSTRAINT appointment_user_fk FOREIGN KEY (user_id) REFERENCES users (id)
);

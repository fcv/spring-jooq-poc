# Spring jOOQ PoC

Proof of Concept project created to experiment on Spring Boot + jOOQ + Kotlin.

## Development

### External Dependencies

This project depends on PostgreSQL. [`docker-compose.yml`](./docker-compose.yml) provides
the default configuration to start up a dockerized PostgreSQL instance.

Note that during tests [Testcontainers](https://www.testcontainers.org/) is used to automatically start up
and shutdown the PostgreSQL instances used by the Test Suites. 

### Database Migrations

This project uses [Flyway](https://flywaydb.org/) to apply database migrations.

There are two ways of applying such migrations:

1. Application startup
2. [Flyway's Gradle Plugin](https://flywaydb.org/documentation/usage/gradle/) via `./gradlew flywayMigrate` command.

### jOOQ code generation

jOOQ "entities" are generated based on the database schema using
[gradle-jooq-plugin](https://github.com/etiennestuder/gradle-jooq-plugin/) via `./gradlew generateJooq` command.

jOOQ generated code is checked in to the version control system.
See the reasoning behind it at 7f6d5661af3cd3631c6c7f1a57c884e171d891f6's message.
